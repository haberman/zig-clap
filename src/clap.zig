const std = @import("std");
const Allocator = std.mem.Allocator;

const print = std.debug.print;
const bufPrint = std.fmt.bufPrint;

fn formatValue(arg_value: ArgumentValue, value: []const u8) ParseError!ArgumentValue {
    return switch (arg_value) {
        .Bool => .{ .Bool = true },
        .Float => .{ .Float = try std.fmt.parseFloat(f64, value) catch ParseError.InvalidArgumentValue },
        .Integer => .{ .Integer = try std.fmt.parseInt(i64, value, 10) catch ParseError.InvalidArgumentValue },
        .String => .{ .String = value },
    };
}

fn getOptionValue(arg_option: ArgumentOption, args: []const []const u8, arg_index: *usize) ParseError!?ArgumentValue {
    const arg_value = arg_option.value;

    while (arg_index.* < args.len) {
        // value can came glued to an arg, as in: `--arg=value` so we start there
        var value_index: usize = arg_index.*;
        var value: []const u8 = args[value_index];

        // the option is a bool, nothing to do…
        if (@enumToInt(arg_value) == 0) {
            arg_index.* += 1;
        } else {
            if (std.mem.lastIndexOfScalar(u8, value, '=')) |idx| {
                value = value[idx + 1 ..];
            } else {
                value_index += 1;
                value = args[value_index];
            }

            arg_index.* = value_index + 1;
        }

        const fmt_value = try formatValue(arg_value, value);
        if (arg_option.validator) |validator| {
            if (validator(fmt_value) == false) return ParseError.InvalidArgumentValue;
        }
        return fmt_value;
    }

    return null;
}

fn getOptionIndex(allocator: Allocator, name: []const u8, options: []const ArgumentOption, alias_lookup: bool) ParseError!usize {
    for (options) |option, i| {
        var buf_size: usize = undefined;
        var prefix: []const u8 = undefined;
        var option_name: []const u8 = undefined;

        if (alias_lookup and option.alias != null) {
            prefix = "-";
            buf_size = 1;
            option_name = option.alias.?;
        } else {
            prefix = "--";
            buf_size = 2;
            option_name = option.name;
        }

        buf_size += option_name.len;
        var buf = allocator.alloc(u8, buf_size) catch return ParseError.InternalFailure;
        defer allocator.free(buf);

        option_name = bufPrint(buf, "{s}{s}", .{ prefix, option_name }) catch return ParseError.InternalFailure;
        if (std.mem.eql(u8, option_name, name)) {
            return i;
        }
    }

    return ParseError.UnkwownArgument;
}

fn getOptionDefault(res: *ParsedResult, options: []const ArgumentOption) ParseError!void {
    for (options) |option| {
        if (!option.positional and res.get(option.name) == null) {
            _ = res.*.put(option.name, option.value) catch return ParseError.InternalFailure;
        }
    }
}

// TODO[@deuglify]:: for now, zig can't parse a runtime fmt like {:n}, hence the above function…
/// Prints `size` amount of space
fn printSpaces(size: usize) void {
    var i: usize = 0;
    while (i < size) : (i += 1) print(" ", .{});
}

// TODO[@incomplete]:: print types and defaults
/// Print the help message following the Python `argparse` style
fn printHelp(
    allocator: Allocator,
    pgm_name: []const u8,
    pgm_description: ?[]const u8,
    options: []const ArgumentOption,
) ParseError!void {
    var positionals = std.ArrayList(ArgumentOption).init(allocator);
    defer positionals.deinit();

    print("usage: {s}", .{pgm_name});
    if (options.len == 0) return;

    var longest_name_size: usize = 0;

    // first loop: print usage and collect / compute infos…
    for (options) |option| {
        if (option.positional) {
            positionals.append(option) catch return ParseError.InternalFailure;
            continue;
        }

        var name_size: usize = option.name.len;
        if (option.alias) |option_alias| {
            name_size += option_alias.len + 2;
            print(" [-{s}]", .{option_alias});
        } else {
            print(" [--{s}]", .{option.name});
        }
        longest_name_size = std.math.max(longest_name_size, name_size);
    }

    if (positionals.items.len > 0) {
        for (positionals.items) |positional| {
            longest_name_size = std.math.max(longest_name_size, positional.name.len - 1);
            print(" {s}", .{positional.name});
        }
    }

    print("\n", .{});

    if (pgm_description) |description| {
        print("\n{s}\n", .{description});
    }

    print("\n", .{});

    // second loop: print positional arguments
    if (positionals.items.len > 0) {
        const maybe_plural = if (positionals.items.len > 1) "s" else "";
        print("positional argument{s}:\n", .{maybe_plural});
        for (positionals.items) |positional| {
            print("  {s}", .{positional.name});
            if (positional.help) |positional_help| {
                printSpaces(longest_name_size - positional.name.len);
                print("{s}", .{positional_help});
            }
            print("\n", .{});
        }
        print("\n", .{});
    }

    // third loop: print optional arguments
    const maybe_plural = if (options.len - positionals.items.len > 1) "s" else "";
    print("optional argument{s}:\n", .{maybe_plural});
    for (options) |option| {
        if (option.positional) continue;

        var name_size: usize = option.name.len + 2;
        if (option.alias) |option_alias| {
            name_size += option_alias.len + 3;
            print("  -{s}, --{s}", .{ option_alias, option.name });
        } else {
            print("  --{s}", .{option.name});
        }
        if (option.help) |option_help| {
            printSpaces(longest_name_size - option.name.len);
            print("{s}", .{option_help});
        }

        print("\n", .{});
    }
}

/// A struct description the program, only used for usage().
pub const Program = struct {
    /// overrides the first argument if set
    name: ?[:0]const u8 = null,
    /// description used when printing usage
    description: ?[:0]const u8 = null,
};

/// Types of argument supported by the parser
pub const ArgumentType = enum {
    Bool,
    Integer,
    Float,
    String,
};

/// Union of a type and a value
pub const ArgumentValue = union(ArgumentType) {
    Bool: ?bool,
    Integer: ?i64,
    Float: ?f64,
    String: ?[]const u8,
};

// TODO[@incomplete] implements input validation
/// A struct that describing a CLI argument
pub const ArgumentOption = struct {
    name: [:0]const u8,
    value: ArgumentValue,

    alias: ?[:0]const u8 = null,
    help: ?[:0]const u8 = null,
    validator: ?fn (ArgumentValue) bool = null,

    positional: bool = false,
    repeat: usize = 1,
};

/// Types of error that could occur during parsing
pub const ParseError = error{
    MissingProgramName,
    MissingPositionalArgument,
    UnkwownArgument,
    InvalidArgumentOption,
    InvalidArgumentValue,
    NotEnoughArgument,
    InternalFailure,
};

/// Shortcut for a parsed result
pub const ParsedResult = std.StringHashMap(ArgumentValue);

/// Parse process command line arguments subject to the options' list
pub fn parseProcessArguments(
    allocator: Allocator,
    comptime pgm: Program,
    comptime options: []const ArgumentOption,
) ParseError!ParsedResult {
    const args = std.process.argsAlloc(allocator) catch return ParseError.InternalFailure;
    return parseArbitraryArguments(allocator, pgm, options, args);
}

/// Parse arbitrary string arguments subject to the options' list
/// Caller owns returned memory
pub fn parseArbitraryArguments(
    allocator: Allocator,
    comptime pgm: Program,
    comptime options: []const ArgumentOption,
    args: []const []const u8,
) ParseError!ParsedResult {
    // Collect program name
    if (pgm.name == null and args.len < 1) return ParseError.MissingProgramName;
    const pgm_name = pgm.name orelse args[0];

    var positional_options = std.ArrayList(ArgumentOption).init(allocator);
    defer positional_options.deinit();

    var names: [options.len][]const u8 = undefined;
    for (options) |option, i| {
        // Ensure an option name does not contain a space
        if (std.mem.lastIndexOfScalar(u8, option.name, ' ') != null) return ParseError.InvalidArgumentOption;

        // Ensure there's no duplicated names in options
        for (names) |name| {
            if (std.mem.eql(u8, name, option.name)) return ParseError.InvalidArgumentOption;
        }

        names[i] = option.name;
        if (option.positional) {
            positional_options.append(option) catch return ParseError.InternalFailure;
        }
    }

    var res = ParsedResult.init(allocator);
    errdefer res.deinit();

    // Iterate over optionals
    var i: usize = 1;
    while (i < args.len) {
        var arg = args[i];

        if (std.mem.eql(u8, arg, "--help")) {
            try printHelp(allocator, pgm_name, pgm.description, options);
            return res;
        }

        if (std.mem.lastIndexOfScalar(u8, arg, '=')) |idx| {
            arg = arg[0..idx];
        }

        var option_index: ?usize = null;
        if (std.mem.startsWith(u8, arg, "--")) {
            option_index = try getOptionIndex(allocator, arg, options, false);
        }
        if (option_index == null and std.mem.startsWith(u8, arg, "-")) {
            option_index = getOptionIndex(allocator, arg, options, true) catch |err| {
                if (err == ParseError.UnkwownArgument and positional_options.items.len > 0) {
                    break; // this can be a negative number as well as the start of a positional argument
                } else return err;
            };
        }

        if (option_index) |idx| {
            const option = options[idx];
            if (try getOptionValue(option, args, &i)) |ov| {
                _ = res.put(option.name, ov) catch return ParseError.InternalFailure;
            }
            continue;
        } else if (i < args.len - positional_options.items.len) {
            // prematured positional
            return ParseError.InvalidArgumentValue;
        } else if (positional_options.items.len == 0) {
            // we shouldn't be there if there's no positional arguments
            return ParseError.InternalFailure;
        } else {
            break;
        }
    }

    // Fills res with default values
    while (res.count() < options.len - positional_options.items.len) {
        try getOptionDefault(&res, options);
    }

    // Iterate over positionals
    while (positional_options.items.len > 0) {
        if (i >= args.len) {
            return ParseError.MissingPositionalArgument;
        }

        const positional = positional_options.orderedRemove(0);
        const value = try formatValue(positional.value, args[i]);
        _ = res.put(positional.name, value) catch return ParseError.InternalFailure;

        i += 1;
    }

    return res;
}
