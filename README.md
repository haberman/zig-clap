# zig CLAP
## Command Line Argument Parser for zig

From [`sample.zig`](https://framagit.org/haberman/zig-clap/-/blob/master/sample.zig):
```rust
const std = @import("std");
const clap = @import("clap");

// A validation function…
fn floatValidator(arg_value: clap.ArgumentValue) bool {
    const value = arg_value.Float orelse return false;
    return value > 1 and value < 5;
}

// All fields of `Program` struct can be omitted.
const pgm = clap.Program{ .name = "./sample", .description = "A simple CLI program with basic setup" };
const options = &[_]clap.ArgumentOption{
    .{ .value = clap.ArgumentValue{ .Bool = null }, .name = "bool", .alias = "b", .help = "A boolean option" },
    .{ .value = clap.ArgumentValue{ .Integer = null }, .name = "int", .alias = "i", .help = "An integer option" },
    .{ .value = clap.ArgumentValue{ .Float = null }, .name = "float", .alias = "f", .help = "A float option that only accept inputs between 1 & 5", .validator = floatValidator },
    .{ .value = clap.ArgumentValue{ .String = null }, .name = "string", .alias = "s", .help = "A string option" },
    .{ .value = clap.ArgumentValue{ .String = null }, .name = "file_positional", .positional = true },
    .{ .value = clap.ArgumentValue{ .Integer = null }, .name = "integer_positional", .positional = true },
};

var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
defer arena.deinit();

// Will return a `ParsedResult`, ie. std.StringHashMap(ArgumentValue) or a `ParseError` in case something went wrong
var res = clap.parseProcessArguments(&arena.allocator, pgm, options) catch |err| {
    print("Error:: {}…\n", .{@errorName(err)});
    return;
};
defer res.deinit();

// Iterate over values…
for (res.items()) |entry| {
    print("'{}' = {}\n", .{ entry.key, entry.value });
}

// or get them individually
const bool_value = res.get("bool").?.Bool.?;

```

Taking inspiration from Python [argparse](https://docs.python.org/3/library/argparse.html), the `--help` argument will print:
```
usage: ./sample [-b] [-i] [-f] [-s] file_positional integer_positional

A simple CLI program with basic setup

positional arguments:
  file_positional
  integer_positional

optional arguments:
  -b, --bool             A boolean option
  -i, --int              An integer option
  -f, --float            A float option that only accept inputs between 1 & 5
  -s, --string           A string option
```

## TODOs

- [x] add inputs validation function
- [ ] add array support
- [x] print outputs of sample
- [x] address `test "Positional arguments"` glitch
- [ ] print types & defaults with `--help`?