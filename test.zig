const std = @import("std");
const print = std.debug.print;
const testing = std.testing;

const clap = @import("clap");
const pgm = clap.Program{};

test "All empty" {
    const options = &[_]clap.ArgumentOption{};

    const args = &[_][]const u8{};
    const res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);

    try testing.expectError(clap.ParseError.MissingProgramName, res);
}

test "Option with space in its name" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Bool = null }, .name = "bo ol" },
    };

    const args = &[_][]const u8{"./test"};
    const res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);

    try testing.expectError(clap.ParseError.InvalidArgumentOption, res);
}

test "Duplicated option's name" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .String = null }, .name = "string" },
        .{ .value = .{ .String = null }, .name = "string" },
    };

    const args = &[_][]const u8{"./test"};
    const res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);

    try testing.expectError(clap.ParseError.InvalidArgumentOption, res);
}

test "Null options with no arguments" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Bool = null }, .name = "bool" },
        .{ .value = .{ .Integer = null }, .name = "int" },
    };

    const args = &[_][]const u8{"./test"};
    var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
    defer res.deinit();

    try testing.expectEqual(@as(usize, 2), res.count());
    try testing.expect(null == res.get("bool").?.Bool);
    try testing.expect(null == res.get("int").?.Integer);
}

test "Default options with no arguments" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Bool = false }, .name = "bool" },
        .{ .value = .{ .Integer = 12 }, .name = "int" },
    };

    const args = &[_][]const u8{"./test"};
    var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
    defer res.deinit();

    try testing.expectEqual(@as(usize, 2), res.count());
    try testing.expectEqual(false, res.get("bool").?.Bool.?);
    try testing.expectEqual(@as(i64, 12), res.get("int").?.Integer.?);
}

test "Options with correct arguments" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Bool = null }, .name = "bool" },
        .{ .value = .{ .Integer = null }, .name = "int" },
        .{ .value = .{ .Float = null }, .name = "float" },
        .{ .value = .{ .String = null }, .name = "str" },
    };

    const args = &[_][]const u8{ "./test", "--float", "-12.34", "--bool", "--int", "124", "--str=test_string" };
    var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
    defer res.deinit();

    try testing.expectEqual(@as(usize, 4), res.count());
    try testing.expectEqual(true, res.get("bool").?.Bool.?);
    try testing.expectEqual(@as(f64, -12.34), res.get("float").?.Float.?);
    try testing.expectEqual(@as(i64, 124), res.get("int").?.Integer.?);
    try testing.expectEqualStrings("test_string", res.get("str").?.String.?);
}

test "Unknow arguments name" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Integer = null }, .name = "int" },
    };

    const args = &[_][]const u8{ "./test", "--unknown", "12" };
    const res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);

    try testing.expectError(clap.ParseError.UnkwownArgument, res);
}

test "Options with invalid argument values" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Float = null }, .name = "float" },
        .{ .value = .{ .Integer = null }, .name = "int" },
    };

    {
        const args = &[_][]const u8{ "./test", "--float", "not_a_float" };
        var res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        try testing.expectError(clap.ParseError.InvalidArgumentValue, res);
    }
    {
        const args = &[_][]const u8{ "./test", "--int", "12.23" };
        var res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        try testing.expectError(clap.ParseError.InvalidArgumentValue, res);
    }
}

test "Options with spaced value arguments" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Float = null }, .name = "float" },
        .{ .value = .{ .String = null }, .name = "string" },
    };

    {
        const args = &[_][]const u8{ "./test", "--float", "12.34 34" };
        const res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        try testing.expectError(clap.ParseError.InvalidArgumentValue, res);
    }

    { // TODO[@incomplete]:: should work differently with upcoming array support
        const args = &[_][]const u8{ "./test", "--float", "34.65", "--string", "foo bar" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 2), res.count());
        try testing.expectEqual(@as(f64, 34.65), res.get("float").?.Float.?);
        try testing.expectEqualStrings("foo bar", res.get("string").?.String.?);
    }
}

test "Aliased option" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .String = null }, .name = "str", .alias = "s" },
    };

    {
        const args = &[_][]const u8{ "./test", "-s", "test_string" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 1), res.count());
        try testing.expectEqualStrings("test_string", res.get("str").?.String.?);
    }

    {
        const args = &[_][]const u8{ "./test", "--str", "test_string" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 1), res.count());
        try testing.expectEqualStrings("test_string", res.get("str").?.String.?);
    }

    { // both a long and an alias for the same option, last takes precedence
        const args = &[_][]const u8{ "./test", "--str=first_string", "-s", "second_string" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 1), res.count());
        try testing.expectEqualStrings("second_string", res.get("str").?.String.?);
    }
}

test "Positional arguments" {
    { // testing strings
        const options = &[_]clap.ArgumentOption{
            .{ .value = .{ .Integer = null }, .name = "int" },
            .{ .value = .{ .String = null }, .name = "positional_string", .positional = true },
        };

        const args = &[_][]const u8{ "./test", "--int=12", "positional_argument" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 2), res.count());
        try testing.expectEqualStrings("positional_argument", res.get("positional_string").?.String.?);
    }
    { // testing int and float
        const options = &[_]clap.ArgumentOption{
            .{ .value = .{ .Integer = null }, .name = "positional_int", .positional = true },
            .{ .value = .{ .Float = null }, .name = "positional_float", .positional = true },
        };

        const args = &[_][]const u8{ "./test", "12", "-14" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 2), res.count());
        try testing.expectEqual(@as(i64, 12), res.get("positional_int").?.Integer.?);
        try testing.expectEqual(@as(f64, -14), res.get("positional_float").?.Float.?);
    }
    { // testing a negative number
        const options = &[_]clap.ArgumentOption{
            .{ .value = .{ .Integer = null }, .name = "positional_int", .positional = true },
        };

        const args = &[_][]const u8{ "./test", "-112" }; // will look for an option named "112" right now
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 1), res.count());
        try testing.expectEqual(@as(i64, -112), res.get("positional_int").?.Integer.?);
    }
}

test "Positional arguments placed on a random slot of options" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .String = "default_string" }, .name = "str" },
        .{ .value = .{ .String = null }, .name = "positional", .positional = true },
        .{ .value = .{ .Integer = null }, .name = "int" },
    };

    const args = &[_][]const u8{ "./test", "--int=12", "positional_string" };
    var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
    defer res.deinit();

    try testing.expectEqual(@as(usize, 3), res.count());
    try testing.expectEqualStrings("default_string", res.get("str").?.String.?);
    try testing.expectEqualStrings("positional_string", res.get("positional").?.String.?);
}

test "Positional argument's value passed before an optional one" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Integer = null }, .name = "int" },
        .{ .value = .{ .String = null }, .name = "str", .alias = "s" },
        .{ .value = .{ .String = null }, .name = "positional", .positional = true },
    };

    const args = &[_][]const u8{ "./test", "--int", "75", "positional_string", "-s", "" };
    var res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
    try testing.expectError(clap.ParseError.InvalidArgumentValue, res);
}

test "Missing positional arguments" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Bool = null }, .name = "bool", .alias = "b" },
        .{ .value = .{ .Float = null }, .name = "float" },
        .{ .value = .{ .String = null }, .name = "string" },
        .{ .value = .{ .String = null }, .name = "positional_string", .positional = true },
    };

    const args = &[_][]const u8{ "./test", "-b" };
    var res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
    try testing.expectError(clap.ParseError.MissingPositionalArgument, res);
}

fn stringValidator(arg_value: clap.ArgumentValue) bool {
    const value = arg_value.String orelse return false;
    return value.len > 6;
}

fn floatValidator(arg_value: clap.ArgumentValue) bool {
    const value = arg_value.Float orelse return false;
    return value < 13;
}

test "Testing validator" {
    const options = &[_]clap.ArgumentOption{
        .{ .value = .{ .Float = null }, .name = "float", .validator = floatValidator },
        .{ .value = .{ .String = null }, .name = "string", .validator = stringValidator },
    };

    { // Error with string validation
        const args = &[_][]const u8{ "./test", "--float=13.34", "--string=foo" };
        var res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        try testing.expectError(clap.ParseError.InvalidArgumentValue, res);
    }
    { // Error with float validation
        const args = &[_][]const u8{ "./test", "--float=13.34", "--string=foo_bar" };
        var res = clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        try testing.expectError(clap.ParseError.InvalidArgumentValue, res);
    }
    { // All good
        const args = &[_][]const u8{ "./test", "--float=12.34", "--string=foo_bar" };
        var res = try clap.parseArbitraryArguments(std.testing.allocator, pgm, options, args);
        defer res.deinit();

        try testing.expectEqual(@as(usize, 2), res.count());
        try testing.expectEqualStrings("foo_bar", res.get("string").?.String.?);
        try testing.expectEqual(@as(f64, 12.34), res.get("float").?.Float.?);
    }
}
