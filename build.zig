const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    const build_mode = b.standardReleaseOptions();
    const clap_lib = b.addStaticLibrary("cli", "src/clap.zig");

    const test_exe = b.addTest("test.zig");
    test_exe.setBuildMode(build_mode);
    test_exe.linkLibrary(clap_lib);
    test_exe.addPackagePath("clap", "src/clap.zig");

    const test_step= b.step("test", "Run the tests");
    test_step.dependOn(&test_exe.step);

    const sample_exe = b.addExecutable("sample", "sample.zig");
    sample_exe.setBuildMode(build_mode);
    sample_exe.linkLibrary(clap_lib);
    sample_exe.addPackagePath("clap", "src/clap.zig");
    sample_exe.setOutputDir("zig-cache");

    const sample_step = b.step("sample", "Run a sample application");
    sample_step.dependOn(&sample_exe.step);
}