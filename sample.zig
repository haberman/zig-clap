const std = @import("std");
const clap = @import("clap");
const print = std.debug.print;

fn floatValidator(arg_value: clap.ArgumentValue) bool {
    const value = arg_value.Float orelse return false;
    return value > 1 and value < 5;
}

pub fn main() clap.ParseError!void {
    const pgm = clap.Program{ .name = "./sample", .description = "A simple CLI program with basic setup" };
    const options = &[_]clap.ArgumentOption{
        .{ .value = clap.ArgumentValue{ .Bool = null }, .name = "bool", .alias = "b", .help = "A boolean option" },
        .{ .value = clap.ArgumentValue{ .Integer = null }, .name = "int", .alias = "i", .help = "An integer option" },
        .{ .value = clap.ArgumentValue{ .Float = null }, .name = "float", .alias = "f", .help = "A float option that only accept inputs between 1 & 5", .validator = floatValidator },
        .{ .value = clap.ArgumentValue{ .String = null }, .name = "string", .alias = "s", .help = "A string option" },
        .{ .value = clap.ArgumentValue{ .String = null }, .name = "file_positional", .positional = true },
        .{ .value = clap.ArgumentValue{ .Integer = null }, .name = "integer_positional", .positional = true },
    };

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    var res = clap.parseProcessArguments(arena.allocator(), pgm, options) catch |err| {
        print("Error:: {s}…\n", .{@errorName(err)});
        return;
    };
    defer res.deinit();

    var it = res.iterator();
    while (it.next()) |entry| {
        print("'{s}' = {s}\n", .{ entry.key_ptr.*, entry.value_ptr.* });
    }
}
